const flatten = require('../flatten');
const { nestedArray } = require('../arrayData');


const flattenedNestedArray = flatten(nestedArray);


console.log('Original nested array:', nestedArray);
console.log('Flattened nested array:', flattenedNestedArray);
