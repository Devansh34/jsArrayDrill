const each =require('../each')
const {items}=require('../arrayData')

each(items,(element,index) => {
    console.log(`Element at index ${index}: ${element}`)
})
