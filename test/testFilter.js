const filter =require('../filter')
const {items}=require('../arrayData')

const filteredElements=filter(items,(element)=>element>2)

console.log(`Orginal items: ${items}`)
console.log(`Filtered items: ${filteredElements}`)