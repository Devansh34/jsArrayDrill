const {items}=require('./arrayData')

function find(elements,cb,target){
    for (let i=0;i<elements.length;i++){
        if(cb(elements[i],target)){
            return true
        }

    }
    return undefined
}

module.exports=find