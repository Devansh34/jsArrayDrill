const {items}=require('./arrayData')

function filter(elements,cb){
    const filteredElements=[]

    for(let i=0;i<elements.length;i++){
        if(cb(elements[i])){
            filteredElements.push(elements[i])
        }
    }
    return filteredElements
}

module.exports=filter