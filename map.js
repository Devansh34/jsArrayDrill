const {items}=require('./arrayData')

function map(elements,cb){
    let results=[]
    for (let i=0;i<elements.length;i++){
        results.push(cb(elements[i],i))
    }
    return results
}

module.exports=map