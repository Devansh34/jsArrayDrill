const {nestedArray}=require('./arrayData')

function flatten(elements){
    const flattened=[]

    function recursiveFunction(arr){
        for (let i=0;i<arr.length;i++){
            if(Array.isArray(arr[i])){
                recursiveFunction(arr[i])
            }else{
                flattened.push(arr[i])
            }
        }
    }
    recursiveFunction(elements)
    console.log(nestedArray)
    return flattened
}

module.exports=flatten